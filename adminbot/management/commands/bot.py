from django.core.management.base import BaseCommand
from django.conf import settings
from datetime import datetime, timedelta
import telebot
import json
from geopy.geocoders import Nominatim
from telebot.types import (
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    KeyboardButton,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
    InputMedia
)

from adminbot.models import Customer, Books, Book_categories, User_cart, Owner, Orders, Admin, Connect_with_us
TOKEN = "1082162530:AAG4gBv0XdJmfWXSh27CYftg2jdllIB8Nq4"

bot = telebot.TeleBot(token=TOKEN)

with open("package.json", "r") as read_file:
    lang = json.load(read_file)


def language_inline_keyboard():
    keyboard = InlineKeyboardMarkup(row_width=1)
    but1 = InlineKeyboardButton("O'zbek", callback_data='uz__')
    but2 = InlineKeyboardButton("Русский", callback_data='ru__')
    keyboard.add(but1, but2)
    return keyboard

def books_inline_keyboard(keyboard):
    keyboards = InlineKeyboardMarkup(row_width=1)
    for key in keyboard:
        but = InlineKeyboardButton(f"{key}", callback_data='uz')
        keyboards.add(but)
    return keyboards


def contact_keyboard():
    keyboard = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    reg_button = KeyboardButton(text= " 📞 Kontaktimni yuborish" , request_contact=True)
    keyboard.add(reg_button)
    return keyboard

def menu_keyboard(user_id):
    customer = Customer.objects.get(user_id=user_id)
    lan = customer.language
    la = lang[f'{lan}']
    key = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    key.add(*[
        KeyboardButton(text=f"{la['meny_key1']}"),
        KeyboardButton(text=f"{la['meny_key2']}"),
        KeyboardButton(text=f"{la['meny_key3']}"),
        KeyboardButton(text=f"{la['meny_key4']}"),
    ])
    return key

def categori_books_keyboard(user_id):
    customer = Customer.objects.get(user_id=user_id)
    user_cart = User_cart.objects.all()
    categories = Book_categories.objects.all()
    books = Books.objects.all()

    lan = customer.language
    la = lang[f'{lan}']
    reply_markup = ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    keybords = []
    for text in categories:
        if len(books.filter(category=text)) > 0:
            keybords.append(*[KeyboardButton(f"{text}")])
    reply_markup.add(*keybords)
    if user_cart.filter(user_id=customer):
        reply_markup.row(*[
            KeyboardButton(f"🛒Savat"),
            KeyboardButton(f"{la['back1']}")
        ])
    else:
        reply_markup.row(*[KeyboardButton(f"{la['back1']}")])

    return reply_markup


def settings_keyboard(user_id):
    customer = Customer.objects.get(user_id=user_id)
    lan = customer.language
    la = lang[f'{lan}']
    keyboard = ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    keyboard.add(*[
        KeyboardButton(text=f"{la['my_data']}"),
        KeyboardButton(text=f"{la['change_lang']}")
    ])
    keyboard.row(*[
        KeyboardButton(text=f"{la['back1']}"),
    ])
    return keyboard


@bot.message_handler(commands=['start'])
def start_regis(message):
    user_id = message.from_user.id
    customer = Customer.objects
    user, _ = Customer.objects.get_or_create(user_id=user_id)
    if customer.filter(user_id=user_id) and customer.get(user_id=user_id).phone_number != None:
        customerr = Customer.objects.get(user_id=user_id)
        lan = customerr.language
        la = lang[f'{lan}']
        user.user_step = 0
        user.save()
        bot.send_message(user_id, f"{la['2']}", reply_markup=menu_keyboard(user_id))
    else:
        bot.send_message(message.from_user.id, f" 👋 Assalom alayeykum hurmatli mijoz "
                                               f"\n\n iltimos ismingizni kiriting 🖋  \n"
                                               f" Misol: Ulugbek Axtamov yoki Ulugbek ")
        user.language = 'uz'
        user.user_step = 1
        user.save()

@bot.message_handler(commands=['menu'])
def start_regis(message):
    user_id = message.from_user.id
    customer = Customer.objects
    user, _ = Customer.objects.get_or_create(user_id=user_id)
    if user.phone_number:
        user.user_step = 0
        user.message_id = 0
        user.save()
        try:
            bot.delete_message(user_id, message_id=message.message_id-1)
            bot.send_message(user_id, "Menu", reply_markup=menu_keyboard(user_id))
        except:
            bot.send_message(user_id, "Menu", reply_markup=menu_keyboard(user_id))
    else:
        bot.send_message(message.from_user.id, f" 👋 Assalom alayeykum hurmatli mijoz "
                                               f"\n\n iltimos ismingizni kiriting 🖋  \n"
                                               f" Misol: Ulugbek Axtamov yoki Ulugbek ")
        user.language = 'uz'
        user.user_step = 1
        user.save()


@bot.message_handler(commands=['cancel'])
def start_regis(message):
    user_id = message.from_user.id
    customer = Customer.objects.get(user_id=user_id)
    user, _ = Customer.objects.get_or_create(user_id=user_id)
    if user.phone_number:
        user.user_step = 0
        user.message_id = 0
        user.save()
        cart = User_cart.objects.filter(user_id=customer)
        cart.delete()
        try:
            bot.delete_message(user_id, message_id=message.message_id-1)
            bot.send_message(user_id, "Bekor qilindi", reply_markup=menu_keyboard(user_id))
        except:
            bot.send_message(user_id, "Bekor qilindi", reply_markup=menu_keyboard(user_id))
    else:
        bot.send_message(message.from_user.id, f" 👋 Assalom alayeykum hurmatli mijoz "
                                               f"\n\n iltimos ismingizni kiriting 🖋  \n"
                                               f" Misol: Ulugbek Axtamov yoki Ulugbek ")
        user.language = 'uz'
        user.user_step = 1
        user.save()

@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):

    user_id = call.message.chat.id
    user, _ = Customer.objects.get_or_create(user_id=user_id)

    customer = Customer.objects.get(user_id=user_id)
    user_cart = User_cart.objects.filter(user_id=customer)



    callback, book_name, num = call.data.split('_')

    if callback == 'ru':
        bot.edit_message_text("Вы выбрали русский язык", chat_id=call.message.chat.id, message_id=call.message.message_id)
        user, _ = Customer.objects.get_or_create(user_id=call.message.chat.id)
        user.language = callback
        if user.user_step == '6':
            user.user_step = 0
            user.save()
            bot.send_message(user_id, "🇷🇺🇷🇺🇷🇺🇷🇺🇷🇺", reply_markup=settings_keyboard(user_id))
        else:
            bot.send_message(call.message.chat.id, 'Отправти свое имя')
            user.user_step = 1
            user.save()

    if callback == 'uz':
        bot.edit_message_text("Siz uzbek tilini tanladingiz", chat_id=call.message.chat.id, message_id=call.message.message_id)
        user, _ = Customer.objects.get_or_create(user_id=call.message.chat.id)
        user.language = callback
        user.save()
        if user.user_step == '6':
            user.user_step = 0
            user.save()
            bot.send_message(user_id, "🇺🇿🇺🇿🇺🇿🇺🇿🇺🇿", reply_markup=settings_keyboard(user_id))
        else:
            bot.send_message(call.message.chat.id, 'Отправти свое имя')
            user.user_step = 1
            user.save()

    if callback == "book":
        book = Books.objects.get(title = book_name)
        lan = customer.language
        la = lang[f'{lan}']
        image = book.image
        category = book.category
        language = book.language
        year = book.year
        author = book.author
        translator = book.translator
        cover = book.cover
        about_the_book = book.about_the_book
        price = book.price

        text = f"{la['book_name']}  {book_name} \n"

        caption = {
            f"{la['book_lange']} " : language,
            f"{la['year']} ": year,
            f"{la['author']} " : f"✍️ {author}",
            f"{la['translator']}" : translator,
            f"{la['cover']} " : cover,
            f"{la['about_the_book']}" : about_the_book,
            f"{la['price']} " : f"{price} so'm"
        }

        for key in caption:
            value = caption.get(key)
            if value != None:
                text += f"{key}: {value} \n"

        keyboard = InlineKeyboardMarkup(row_width=1)
        but1 = InlineKeyboardButton("✅ Buyurtma qilish", callback_data=f'buyurtma_{book_name}_')
        but2 = InlineKeyboardButton(" 🔙 Orqaga", callback_data=f'back_{book.category}_')
        keyboard.add(but1, but2)

        bot.delete_message(call.message.chat.id, message_id=call.message.message_id)
        bot.send_photo(call.message.chat.id, photo=image, caption=text, reply_markup=keyboard)
        user.user_step = 9
        user.save()

    elif callback == "back" and user.user_step == '8':
        if book_name:
            book = Books.objects.get(title=book_name)

            keyboard = InlineKeyboardMarkup(row_width=1)
            but1 = InlineKeyboardButton("✅ Buyurtma qilish", callback_data=f'buyurtma_{book_name}_')
            but2 = InlineKeyboardButton("🔙 Orqaga", callback_data=f'back_{book.category}_')
            keyboard.add(but1, but2)

            bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id, reply_markup=keyboard)
            user.user_step = 9
            user.save()

            customer = Customer.objects.get(user_id=user_id)
            book = Books.objects.get(title=book_name)
            cart = User_cart.objects.get(user_id=customer, book_name=book)
            cart.delete()

        else:
            bot.delete_message(user_id, message_id=call.message.message_id)
            bot.send_message(user_id, "🔙 Orqaga", reply_markup=categori_books_keyboard(user_id))
            user.user_step = 7
            user.save()

    elif callback == "back" and user.user_step == '11':
        bot.delete_message(user_id, message_id=call.message.message_id)
        bot.send_message(user_id, "🔙 Orqaga", reply_markup=categori_books_keyboard(user_id))
        user.user_step = 7
        user.save()

    elif callback == "back" and user.user_step == '9':
        category = Book_categories.objects.get(book_categories_name=book_name)
        bookss = Books.objects.filter(category=category)
        list = []
        list1 = []
        for book in bookss:
            list1.append(book.title)
            if len(list1) == 10:
                list.append(list1)
                list1 = []
        list.append(list1)
        lens = len(list)
        keyboards = InlineKeyboardMarkup(row_width=1)
        for book in list[0]:
            but = InlineKeyboardButton(f"📘 {book}", callback_data=f'book_{book}_0')
            keyboards.add(but)
        if lens > 1:
            keyboards.row(*[
                InlineKeyboardButton(f"⏪", callback_data=f'backnextt_{category}_0'),
                InlineKeyboardButton(f"{1}/{lens}", callback_data=f'book_{category}_0'),
                InlineKeyboardButton(f"⏩", callback_data=f'nextt_{category}_0'),
            ])
        if user_cart.filter(user_id=customer):
            keyboards.row(*[InlineKeyboardButton(f"🛒 Savat", callback_data=f"savat_1_")])
        keyboards.add(*[InlineKeyboardButton("🔙 Orqaga", callback_data=f"back_{category}_")])

        bot.edit_message_media(media=InputMedia(type='photo', media=category.image), chat_id=call.message.chat.id,
                               message_id=call.message.message_id, reply_markup=keyboards)

        bot.edit_message_caption(category.book_categories_name, user_id, message_id=call.message.message_id, reply_markup=keyboards)

        user.user_step = 11
        user.save()

    if callback == "buyurtma":

        customer = Customer.objects.get(user_id=user_id)
        book = Books.objects.get(title=book_name)
        user_cart, _ = User_cart.objects.get_or_create(
             user_id=customer, book_name=book)
        user_cart.image = book.image
        user_cart.save()
        reply_markup = InlineKeyboardMarkup(row_width=3)
        reply_markup.add(*[
            InlineKeyboardButton(text='-', callback_data=f'decrement_{book_name}_'),
            InlineKeyboardButton(text=f'{user_cart.qty}', callback_data=f'soni_{book_name}_'),
            InlineKeyboardButton(text='+', callback_data=f'increment_{book_name}_'),
            InlineKeyboardButton(text="☑️Buyurtma berish", callback_data=f"Savatga_{book_name}_")
        ])
        reply_markup.row(*[
            InlineKeyboardButton(text="🔙 Orqaga", callback_data=f"back_{book_name}_")
        ])
        bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id, reply_markup=reply_markup)
        user.user_step = 8
        user.save()

    elif callback == "increment":
        customer = Customer.objects.get(user_id=user_id)
        book = Books.objects.get(title=book_name)

        user_cart, _ = User_cart.objects.get_or_create(
            user_id=customer, book_name=book)
        user_cart.qty += 1
        user_cart.save()
        reply_markup = InlineKeyboardMarkup(row_width=3)
        reply_markup.add(*[
            InlineKeyboardButton(text='-', callback_data=f'decrement_{book_name}_'),
            InlineKeyboardButton(text=f"{user_cart.qty}", callback_data=f'soni_{book_name}_'),
            InlineKeyboardButton(text="+", callback_data=f"increment_{book_name}_"),
            InlineKeyboardButton(text="☑️Buyurtma berish", callback_data=f"Savatga_{book_name}_")
        ])
        reply_markup.row(*[
            InlineKeyboardButton(text="🔙 Orqaga", callback_data=f"back_{book_name}_")
        ])
        bot.edit_message_reply_markup(
            call.from_user.id,
            call.message.message_id,
            reply_markup=reply_markup
        )

    elif callback == "decrement":

        customer = Customer.objects.get(user_id=user_id)
        book = Books.objects.get(title=book_name)
        user_cart, _ = User_cart.objects.get_or_create(
            user_id=customer, book_name=book)
        if user_cart.qty >= 2:
            user_cart.qty -= 1
            user_cart.save()
            reply_markup = InlineKeyboardMarkup(row_width=3)
            reply_markup.add(*[
                InlineKeyboardButton(text='-', callback_data=f'decrement_{book_name}_'),
                InlineKeyboardButton(text=f"{user_cart.qty}", callback_data=f'soni_{book_name}_'),
                InlineKeyboardButton(text="+", callback_data=f"increment_{book_name}_"),
                InlineKeyboardButton(text="☑️Buyurtma berish", callback_data=f"Savatga_{book_name}_")
            ])
            reply_markup.row(*[
                InlineKeyboardButton(text="🔙 Orqaga", callback_data=f"back_{book_name}_")
            ])
            bot.edit_message_reply_markup(
                call.from_user.id,
                call.message.message_id,
                reply_markup=reply_markup
            )


    elif callback == "Savatga":

        customer = Customer.objects.get(user_id=user_id)
        book = Books.objects.get(title=book_name)
        user_cart, _ = User_cart.objects.get_or_create(
            user_id=customer, book_name=book)

        reply_markup = InlineKeyboardMarkup(row_width=1)
        reply_markup.add(*[
            InlineKeyboardButton(text="🔄 Buyurtmani davom etirish", callback_data=f'menu_{book_name}_'),
            InlineKeyboardButton(text="✅ Buyurtmani tastiqlash", callback_data=f'savat_{book_name}_'),
        ])
        bot.edit_message_reply_markup(
            call.from_user.id,
            call.message.message_id,
            reply_markup=reply_markup)

    elif callback == "savat":
        s = 0
        n = len(user_cart)
        qty = user_cart[s].qty
        book_name = user_cart[s].book_name
        text = f"📕: {user_cart[s].book_name}\n\n" \
               f"💴 narxi: {qty} ta x {book_name.price} = {user_cart[s].final_price} so'm"
        final_price = 0
        for price in user_cart:
            final_price += price.final_price
        keyboard = InlineKeyboardMarkup(row_width=4)
        keyboard.add(*[
            InlineKeyboardButton(text='-', callback_data=f'decrementt_{book_name}_{s}'),
            InlineKeyboardButton(text=f'{qty}', callback_data=f'soni_{book_name}_'),
            InlineKeyboardButton(text='+', callback_data=f'incrementt_{book_name}_{s}'),
            InlineKeyboardButton(text='❌', callback_data=f'delet_{book_name}_{s}'),
        ])

        keyboard.add(*[
            InlineKeyboardButton(text='⏪', callback_data=f'backnext_{book_name}_{s}'),
            InlineKeyboardButton(text=f'{s + 1}/{n}', callback_data="__"),
            InlineKeyboardButton(text='⏩', callback_data=f'next_{book_name}_{s}'),
        ])
        keyboard.row(*[
            InlineKeyboardButton(text=f'💴 Umumiy narx: {final_price} so\'m', callback_data="__")
        ])

        keybord = ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
        keybord.add(*[KeyboardButton(text="✅ Tasdiqlash"),
                      KeyboardButton(text="🛒 ❌ Savatni tozalash")])
        keybord.add(*[KeyboardButton(text=" 🏠 Menu")])
        bot.delete_message(user_id, message_id=call.message.message_id)
        bot.send_message(user_id, "🛒 savat", reply_markup=keybord)
        user.message_id = call.message.message_id + 2
        user.user_step = 10
        user.save()

        bot.send_photo(user_id, photo=user_cart[s].image, caption=text, reply_markup=keyboard)


    elif callback == "otpravit":
        admin_id = Admin.objects.all()

        customer = Customer.objects.get(user_id=user_id)
        owner, _ = Owner.objects.get_or_create(customer=customer)
        order, _ = Orders.objects.get_or_create(name=customer,
                                                status="Qabul qilish kutilmoqda 🔄",
                                                orders=owner.products,
                                                phone_number=customer.phone_number,
                                                final_price=owner.final_price,
                                                adress=owner.adress,
                                                longitude=owner.longitude,
                                                latitude=owner.latitude)
        order.save()
        owner.delete()
        time = (order.modified + timedelta(hours=5)).strftime(f"%Y-%m-%d %H:%M:%S")
        zakaz = f"🛍 buyurtma! \n" \
                f"🆔: {order.id} \n\n" \
                f"🚹 status: {order.status}\n" \
                f"🕔 Yaratilgan vaqti: {time}\n\n" \
                f"👤 mijoz ismi: {customer.name} \n" \
                f"📞 telefon raqami: {customer.phone_number}\n\n" \
                f"📉 manzil: {owner.adress}\n" \
                f"📚 mahsulot: \n\n {owner.products}\n" \
                f"💰 umumiy narx:  {owner.final_price} so'm\n"
        for i in user_cart:
            i.delete()

        keyboard = InlineKeyboardMarkup(row_width=1)
        keyboard.add(*[
            InlineKeyboardButton("✅ Qabul qilish", callback_data=f"qabul_{customer}_{order.id}"),
            InlineKeyboardButton("❌ Bekor qilish", callback_data=f"bekor_{customer}_{order.id}"),
        ])

        for i in admin_id:
            bot.send_location(chat_id=i.admin_id.user_id, longitude=owner.longitude, latitude=owner.latitude)
            bot.send_message(chat_id=i.admin_id.user_id, text=zakaz, reply_markup=keyboard)
        bot.edit_message_reply_markup(user_id, message_id=call.message.message_id, reply_markup=None)
        bot.send_message(user_id, "Buyurtmangiz administratorga yetkazildi.\n "
                                  "Tez orada siz bilan bog'lanamiz 😉 \n\n  "
                                  "Buyurtmangiz uchun tashakkur !", reply_markup=menu_keyboard(user_id))

    elif callback == "qabul":
        try:
            order, _ = Orders.objects.get_or_create(id=num)
            status = order.status

            order.status = "✅ Qabulqilindi"
            order.save()
            time = (order.modified + timedelta(hours=5)).strftime(f"%Y-%m-%d %H:%M:%S")
            zakaz = f"🛍 buyurtma! \n" \
                    f"🆔: {order.id} \n\n" \
                    f"🚹 status: {order.status}\n" \
                    f"🕔 Yaratilgan vaqti: {time}\n\n" \
                    f"👤 mijoz ismi: {order.name} \n" \
                    f"📞 telefon raqami: {order.phone_number}\n\n" \
                    f"📉 manzil: {order.adress}\n\n" \
                    f"📚 mahsulot: \n\n {order.orders}\n" \
                    f"💰 umumiy narx:  {order.final_price} so'm\n"
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text=zakaz,
                                  reply_markup='')
            if status == "Qabul qilish kutilmoqda 🔄":
                bot.send_message(order.name.user_id, zakaz)
        except:
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Afsus buyurtma oldinroq bekor qilingan ☹️",
                                  reply_markup='')

    elif callback == "bekor":
        try:
            order, _ = Orders.objects.get_or_create(id=num)
            status = order.status
            order.status = "Rad etildi ❌❌❌"
            time = (order.modified + timedelta(hours=5)).strftime(f"%Y-%m-%d %H:%M:%S")
            zakaz = f"🛍 buyurtma! \n" \
                    f"🆔: {order.id} \n\n" \
                    f"🚹 status: {order.status}\n" \
                    f"🕔 Yaratilgan vaqti: {time}\n\n" \
                    f"👤 mijoz ismi: {order.name} \n" \
                    f"📞 telefon raqami: {order.phone_number}\n\n" \
                    f"📉 manzil: {order.adress}\n\n" \
                    f"📚 mahsulot: \n\n {order.orders}\n" \
                    f"💰 umumiy narx:  {order.final_price} so'm\n"
            if status == "Qabul qilish kutilmoqda 🔄":
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text=zakaz,
                                      reply_markup='')
                bot.send_message(order.name.user_id, zakaz )
                order.delete()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Uzur buyurtma oldinroq qabul qilingan ️🤷🏻‍♂️",
                                      reply_markup='')
        except:
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Buyurtma oldinroq bekor qilingan ☹️",
                                  reply_markup='')

    elif callback == "menu":
        customer = Customer.objects.get(user_id=user_id)
        book = Books.objects.get(title=book_name)
        user_cart = User_cart.objects.get(user_id=customer, book_name=book)

        bot.delete_message(call.message.chat.id, message_id=call.message.message_id)
        bot.send_message(call.message.chat.id, f"📘 {book_name}  kitobi 🛒 savatga qo'shildi", reply_markup=categori_books_keyboard(user_id))
        user.user_step = 7
        user.save()

    user_id = call.message.chat.id
    user, _ = Customer.objects.get_or_create(user_id=user_id)

    if callback == "next":
        s = int(num) + 1
        n = len(user_cart)
        if s == n:
            s = 0
        qty = user_cart[s].qty
        book_name = user_cart[s].book_name
        text = f"📕: {user_cart[s].book_name}\n\n" \
               f"💴 narxi: {qty} ta x {book_name.price} = {user_cart[s].final_price} so'm"
        final_price = 0
        for price in user_cart:
            final_price += price.final_price
        keyboard = InlineKeyboardMarkup(row_width=4)
        keyboard.add(*[
            InlineKeyboardButton(text='-', callback_data=f'decrementt_{book_name}_{s}'),
            InlineKeyboardButton(text=f'{qty}', callback_data=f'soni_{book_name}_'),
            InlineKeyboardButton(text='+', callback_data=f'incrementt_{book_name}_{s}'),
            InlineKeyboardButton(text='❌', callback_data=f'delet_{book_name}_{s}'),
        ])
        keyboard.add(*[
            InlineKeyboardButton(text='⏪', callback_data=f'backnext_{book_name}_{s}'),
            InlineKeyboardButton(text=f'{s + 1}/{n}', callback_data="__"),
            InlineKeyboardButton(text='⏩', callback_data=f'next_{book_name}_{s}'),
        ])
        keyboard.row(*[
            InlineKeyboardButton(text=f'💴 Umumiy narx: {final_price} so\'m', callback_data="__")
        ])
        bot.edit_message_media(media=InputMedia(type='photo', media=user_cart[s].image), chat_id=call.message.chat.id,
                               message_id=call.message.message_id, reply_markup=keyboard)
        user.message_id = call.message.message_id
        user.save()
        bot.edit_message_caption(text, user_id, message_id=call.message.message_id, reply_markup=keyboard)

    elif callback == "backnext":
            s = int(num) - 1
            n = len(user_cart)
            if s < 0:
                s = n - 1
            qty = user_cart[s].qty
            book_name = user_cart[s].book_name
            text = f"📕: {user_cart[s].book_name}\n\n" \
                   f"💴 narxi: {qty} ta x {book_name.price} = {user_cart[s].final_price} so'm"
            final_price = 0
            for price in user_cart:
                final_price += price.final_price
            keyboard = InlineKeyboardMarkup(row_width=4)
            keyboard.add(*[
                InlineKeyboardButton(text='-', callback_data=f'decrementt_{book_name}_{s}'),
                InlineKeyboardButton(text=f'{qty}', callback_data=f'soni_{book_name}_'),
                InlineKeyboardButton(text='+', callback_data=f'incrementt_{book_name}_{s}'),
                InlineKeyboardButton(text='❌', callback_data=f'delet_{book_name}_{s}'),
            ])
            keyboard.add(*[
                InlineKeyboardButton(text='⏪', callback_data=f'backnext_{book_name}_{s}'),
                InlineKeyboardButton(text=f'{s + 1}/{n}', callback_data="__"),
                InlineKeyboardButton(text='⏩', callback_data=f'next_{book_name}_{s}'),
            ])
            keyboard.row(*[
                InlineKeyboardButton(text=f'💴 Umumiy narx: {final_price} so\'m', callback_data="__")
            ])
            bot.edit_message_media(media=InputMedia(type='photo', media=user_cart[s].image), chat_id=call.message.chat.id,
                                   message_id=call.message.message_id, reply_markup=keyboard)
            user.message_id = call.message.message_id
            user.save()
            bot.edit_message_caption(text, user_id, message_id=call.message.message_id, reply_markup=keyboard)

    elif callback == "decrementt":
        books = Books.objects.get(title=book_name)
        cart = user_cart.get(user_id=customer, book_name=books)
        if cart.qty > 1:
            cart.qty -= 1
            cart.save()
            s = int(num)
            n = len(user_cart)
            qty = user_cart[s].qty
            book_name = user_cart[s].book_name
            text = f"📕: {user_cart[s].book_name}\n\n" \
                   f"💴 narxi: {qty} ta x {book_name.price} = {user_cart[s].final_price} so'm"
            final_price = 0
            for price in user_cart:
                final_price += price.final_price
            keyboard = InlineKeyboardMarkup(row_width=4)
            keyboard.add(*[
                InlineKeyboardButton(text='-', callback_data=f'decrementt_{book_name}_{s}'),
                InlineKeyboardButton(text=f'{qty}', callback_data=f'soni_{book_name}_'),
                InlineKeyboardButton(text='+', callback_data=f'incrementt_{book_name}_{s}'),
                InlineKeyboardButton(text='❌', callback_data=f'delet_{book_name}_{s}'),
            ])
            keyboard.add(*[
                InlineKeyboardButton(text='⏪', callback_data=f'backnext_{book_name}_{s}'),
                InlineKeyboardButton(text=f'{s + 1}/{n}', callback_data="__"),
                InlineKeyboardButton(text='⏩', callback_data=f'next_{book_name}_{s}'),
            ])
            keyboard.row(*[
                InlineKeyboardButton(text=f'💴 Umumiy narx: {final_price} so\'m', callback_data="__")
            ])
            user.message_id = call.message.message_id
            user.save()
            bot.edit_message_caption(text, user_id, message_id=call.message.message_id, reply_markup=keyboard)

    elif callback == "incrementt":
        books = Books.objects.get(title=book_name)
        cart = user_cart.get(user_id=customer, book_name=books)
        cart.qty += 1
        cart.save()
        s = int(num)
        n = len(user_cart)
        qty = user_cart[s].qty
        book_name = user_cart[s].book_name
        text = f"📕: {user_cart[s].book_name}\n\n" \
               f"💴 narxi: {qty} ta x {book_name.price} = {user_cart[s].final_price} so'm"
        final_price = 0
        for price in user_cart:
            final_price += price.final_price
        keyboard = InlineKeyboardMarkup(row_width=4)
        keyboard.add(*[
            InlineKeyboardButton(text='-', callback_data=f'decrementt_{book_name}_{s}'),
            InlineKeyboardButton(text=f'{qty}', callback_data=f'soni_{book_name}_'),
            InlineKeyboardButton(text='+', callback_data=f'incrementt_{book_name}_{s}'),
            InlineKeyboardButton(text='❌', callback_data=f'delet_{book_name}_{s}'),
        ])
        keyboard.add(*[
            InlineKeyboardButton(text='⏪', callback_data=f'backnext_{book_name}_{s}'),
            InlineKeyboardButton(text=f'{s + 1}/{n}', callback_data="__"),
            InlineKeyboardButton(text='⏩', callback_data=f'next_{book_name}_{s}'),
        ])
        keyboard.row(*[
            InlineKeyboardButton(text=f'💴 Umumiy narx: {final_price} so\'m', callback_data="__")
        ])
        user.message_id = call.message.message_id
        user.save()
        bot.edit_message_caption(text, user_id, message_id=call.message.message_id, reply_markup=keyboard)

    if callback == "delet":
        s = int(num)
        books = Books.objects.get(title=book_name)
        cart = User_cart.objects.get(user_id=customer, book_name=books)
        cart.delete()
        n = len(user_cart)
        if n > 0:
            if s > 0:
                s -= 1
            qty = user_cart[s].qty
            book_name = user_cart[s].book_name
            text = f"📕: {user_cart[s].book_name}\n\n" \
                   f"💴 narxi: {qty} ta x {book_name.price} = {user_cart[s].final_price} so'm"
            final_price = 0
            for price in user_cart:
                final_price += price.final_price
            keyboard = InlineKeyboardMarkup(row_width=4)
            keyboard.add(*[
                InlineKeyboardButton(text='-', callback_data=f'decrementt_{book_name}_{s}'),
                InlineKeyboardButton(text=f'{qty}', callback_data=f'soni_{book_name}_'),
                InlineKeyboardButton(text='+', callback_data=f'incrementt_{book_name}_{s}'),
                InlineKeyboardButton(text='❌', callback_data=f'delet_{book_name}_{s}'),
            ])
            keyboard.add(*[
                InlineKeyboardButton(text='⏪', callback_data=f'backnext_{book_name}_{s}'),
                InlineKeyboardButton(text=f'{s + 1}/{n}', callback_data="__"),
                InlineKeyboardButton(text='⏩', callback_data=f'next_{book_name}_{s}'),
            ])
            keyboard.row(*[
                InlineKeyboardButton(text=f'💴 Umumiy narx: {final_price} so\'m', callback_data="__")
            ])
            bot.edit_message_media(media=InputMedia(type='photo', media=user_cart[s].image),
                                   chat_id=call.message.chat.id,
                                   message_id=call.message.message_id, reply_markup=keyboard)
            user.message_id = call.message.message_id
            user.save()
            bot.edit_message_caption(text, user_id, message_id=call.message.message_id, reply_markup=keyboard)

        else:
            bot.delete_message(user_id, message_id=call.message.message_id)
            bot.send_message(user_id, " 🛒 ❎ Savatingiz uchirildi", reply_markup=menu_keyboard(user_id))

    elif callback == "nextt":
        category = Book_categories.objects.get(book_categories_name=book_name)
        bookss = Books.objects.filter(category=category)
        list = []
        list1 = []
        for book in bookss:
            list1.append(book.title)
            if len(list1) == 10:
                list.append(list1)
                list1 = []
        list.append(list1)
        lens = len(list)
        s = int(num) + 1
        if s == lens:
            s = 0
        keyboards = InlineKeyboardMarkup(row_width=1)
        for book in list[s]:
            but = InlineKeyboardButton(f"📘 {book}", callback_data=f'book_{book}_0')
            keyboards.add(but)
        if lens > 1:
            keyboards.row(*[
                InlineKeyboardButton(f"⏪", callback_data=f'backnextt_{category}_{s}'),
                InlineKeyboardButton(f"{s+1}/{lens}", callback_data=f'__'),
                InlineKeyboardButton(f"⏩", callback_data=f'nextt_{category}_{s}'),
            ])
        if user_cart.filter(user_id=customer):
            keyboards.row(*[InlineKeyboardButton(f"🛒 Savat", callback_data=f"savat_1_")])
        keyboards.add(*[InlineKeyboardButton("🔙 Orqaga", callback_data=f"back__")])

        bot.edit_message_reply_markup(user_id, message_id=call.message.message_id, reply_markup=keyboards)

    elif callback == "backnextt":
        category = Book_categories.objects.get(book_categories_name=book_name)
        bookss = Books.objects.filter(category=category)
        list = []
        list1 = []
        for book in bookss:
            list1.append(book.title)
            if len(list1) == 10:
                list.append(list1)
                list1 = []
        list.append(list1)
        lens = len(list)
        s = int(num)
        if s == 0:
            s = lens - 1
        else:
            s -= 1

        keyboards = InlineKeyboardMarkup(row_width=1)
        for book in list[s]:
            but = InlineKeyboardButton(f"📘 {book}", callback_data=f'book_{book}_0')
            keyboards.add(but)
        if lens > 1:
            keyboards.row(*[
                InlineKeyboardButton(f"⏪", callback_data=f'backnextt_{category}_{s}'),
                InlineKeyboardButton(f"{s + 1}/{lens}", callback_data=f'__'),
                InlineKeyboardButton(f"⏩", callback_data=f'nextt_{category}_{s}'),
            ])
        if user_cart.filter(user_id=customer):
            keyboards.row(*[InlineKeyboardButton(f"🛒 Savat", callback_data=f"savat_1_")])
        keyboards.add(*[InlineKeyboardButton("🔙 Orqaga", callback_data=f"back__")])

        bot.edit_message_reply_markup(user_id, message_id=call.message.message_id, reply_markup=keyboards)


@bot.message_handler(content_types=["location"])
def location(message):
    user_id = message.chat.id
    customer = Customer.objects.get(user_id=user_id)
    owner, _ = Owner.objects.get_or_create(customer=customer)
    user_cart = User_cart.objects.filter(user_id=customer)
    if message.location is not None:
        geolocator = Nominatim(user_agent="geoapiExercises")

        location = geolocator.geocode(str(message.location.latitude) + " " + str(message.location.longitude))
        owner.adress = location
        owner.latitude = message.location.latitude
        owner.longitude = message.location.longitude
        owner.save()
        text = f"🛍 buyurtma! \n\n" \
               f"👤 mijoz ismi: {customer.name} \n" \
               f"📞 telefon raqami: {customer.phone_number}\n\n" \
               f"📉 manzil: {owner.adress}\n\n" \
               f"📚 mahsulot: \n\n {owner.products}\n\n" \
               f"💰 umumiy narx: {owner.final_price} so'm\n"

        bot.send_message(user_id, " 📍 Manzilingiz qabul qilindi ", reply_markup=ReplyKeyboardRemove())
        keyboar = InlineKeyboardMarkup(row_width=1)
        keyboar.add(*[InlineKeyboardButton(text="↗️ buyurtmani yuborish", callback_data="otpravit_1_")])
        bot.send_message(user_id, text, reply_markup=keyboar)


@bot.message_handler(func=lambda message: True, content_types=['text', 'contact'])
def regist(message):

    categories = Book_categories.objects.all()
    books = Books.objects.all()

    user_id = message.chat.id
    user, _ = Customer.objects.get_or_create(user_id=user_id)

    customer = Customer.objects.get(user_id=user_id)
    user_cart = User_cart.objects.filter(user_id=customer)
    lan = customer.language
    la = lang[f'{lan}']
    if user.user_step == '1':
        name = message.text.replace(" ", "")
        name = all(map(str.isalpha, name))
        if name == True:
            user.name = message.text
            bot.send_message(user_id, text=f"{la['name_regis']} {message.text}")
            user.save()
            bot.send_message(user_id, text=f"{user.name} {la['phone_regis']}", reply_markup=contact_keyboard())
            user.user_step = 2
            user.save()
        else:
            bot.send_message(user_id, f"{la['name_regis1']}")
    elif user.user_step == '2':
        if message.contact:
            phone = message.contact.phone_number
            if phone[0] == "+":
                user.phone_number = message.contact.phone_number
            else:
                phone = "+" + str(phone)
                user.phone_number = phone
            user.user_step = 0
            try:
                user.save()
                bot.send_message(chat_id=user_id, text=f"{la['1']}", reply_markup=menu_keyboard(user_id))
            except:
                bot.send_message(chat_id=user_id, text=f"📱 Bu raqam registratsiyadan o'tgan ☹️\n"
                                                       f" iltimos tekshirib qayta yuboring 🔄")
        elif message.text:
            phone = message.text.replace(' ', '').replace('+', '')
            if phone.isdigit() == True:
                if phone[0: 3] == '998' and len(phone) == 12:
                    phone = "+" + str(phone)
                    user.phone_number = phone
                    user.user_step = 0
                    try:
                        user.save()
                        bot.send_message(chat_id=user_id, text=f"{la['1']}", reply_markup=menu_keyboard(user_id))
                    except:
                        bot.send_message(chat_id=user_id,
                                         text=f"📱 Bu raqam registratsiyadan o'tgan ☹️\n"
                                              f" iltimos tekshirib qayta yuboring 🔄")
                elif len(phone) == 9:
                    phone = "+998" + str(phone)
                    user.phone_number = phone
                    user.user_step = 0
                    try:
                        user.save()
                        bot.send_message(chat_id=user_id, text=f"{la['1']}", reply_markup=menu_keyboard(user_id))
                    except:
                        bot.send_message(chat_id=user_id,
                                         text=f"📱 Bu raqam registratsiyadan o'tgan ☹️\n"
                                              f" iltimos tekshirib qayta yuboring 🔄")

                else: bot.send_message(user_id, text=f" {user.name} {la['phone_regis1']}", reply_markup=contact_keyboard())
            else: bot.send_message(user_id, text=f"{user.name} {la['phone_regis1']} ", reply_markup=contact_keyboard())
        else: bot.send_message(user_id, text=f"{user.name} {la['phone_regis1']} {user.name}", reply_markup=contact_keyboard())

    elif message.text == f"{la['meny_key1']}":
        user.user_step = 7
        user.save()
        bot.send_message(message.chat.id, text=' ↕ kerakli kategoriyani tanlang', reply_markup=categori_books_keyboard(user_id))

    elif user.user_step == '7' and message.text != "🛒Savat":
        try:
            if message.text == f"{la['back1']}":
                bot.send_message(user_id, message.text, reply_markup=menu_keyboard(user_id))
                user.user_step = 0
                user.save()
            else:
                category = Book_categories.objects.get(book_categories_name=message.text)
                bookss = Books.objects.filter(category=category)
                list = []
                list1 = []
                for book in bookss:
                    list1.append(book.title)
                    if len(list1) == 10:
                        list.append(list1)
                        list1 = []
                list.append(list1)
                lens = len(list)
                keyboards = InlineKeyboardMarkup(row_width=1)
                for book in list[0]:
                    but = InlineKeyboardButton(f"📕 {book}", callback_data=f'book_{book}_0')
                    keyboards.add(but)
                if lens > 1:
                    keyboards.row(*[
                        InlineKeyboardButton(f"⏪", callback_data=f'backnextt_{category}_0'),
                        InlineKeyboardButton(f"{1}/{lens}", callback_data=f'__'),
                        InlineKeyboardButton(f"⏩", callback_data=f'nextt_{category}_0'),
                    ])
                if user_cart.filter(user_id=customer):
                    keyboards.row(*[InlineKeyboardButton(f"🛒Savat", callback_data=f"savat_1_")])
                keyboards.add(*[InlineKeyboardButton("🔙 orqaga", callback_data=f"back__")])
                bot.send_message(user_id, message.text, reply_markup=ReplyKeyboardRemove())
                bot.send_photo(user_id, photo=category.image, caption=f"📚 {message.text}", reply_markup=keyboards)
                user.user_step = 8
                user.save()
        except:
            bot.send_message(user_id, text=' ↕ kerakli kategoriyani tanlang')

    elif user.user_step == '8':
        bot.delete_message(user_id, message_id=message.message_id)

    elif message.text == f"{la['meny_key2']}":
        orders = Orders.objects.filter(name=customer)
        if orders:
            for i in orders:
                time = (i.modified + timedelta(hours=5)).strftime(f"%Y-%m-%d %H:%M:%S")
                text = f"🆔 {i.id}\n\n" \
                       f"🚹 status: {i.status}\n" \
                       f"🕔 Yaratilgan vaqti: {time}\n\n" \
                       f"👤 mijoz ismi: {i.name.name} \n" \
                       f"📞 telefon raqami: {i.phone_number}\n\n" \
                       f"📉 manzil: {i.adress}\n\n" \
                       f"📚 mahsulot: \n\n {i.orders}\n" \
                       f"💰 umumiy narx: {i.final_price} so'm \n"

                bot.send_message(user_id, text)
        else:
            bot.send_message(user_id, "Sizda buyurtmalar mavzud emas ☹️")

    elif message.text == f"{la['meny_key3']}":
        try:
            connect_whit_us = Connect_with_us.objects.all()
            bot.send_message(user_id, text=connect_whit_us[0].connect_with_us)
        except:
            bot.send_message(user_id, text="tez orada yuklanadi 🙈")

    elif message.text == f"{la['meny_key4']}":
        bot.send_message(user_id, f"{la['meny_key4']}", reply_markup=settings_keyboard(user_id))

    elif message.text == f"{la['change_lang']}":
        bot.send_message(user_id, f"Tez orada yuklanadi 🇷🇺 🇺🇿")
        # user.user_step = 6
        # user.save()
        # bot.send_message( user_id, f"{la['Select_lang']}", reply_markup=language_inline_keyboard())

    elif message.text == f"{la['my_data']}":
        keyboard = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
        keyboard.add(*[KeyboardButton(f"{la['change_name']}"),
                      KeyboardButton(f"{la['change_phone']}"),
                      KeyboardButton(f"{la['back1']}")])
        user.user_step = 3
        user.save()
        bot.send_message(user_id, f"{la['Personal_information']} \n\n {la['last_first_name']}: {user.name}"
                                  f"\n\n{la['phone']}: {user.phone_number}", reply_markup=keyboard)
    elif message.text == f"{la['back1']}" and (user.user_step == '3' or user.user_step == '4'):
        user.user_step = 0
        user.save()
        bot.send_message(user_id, message.text, reply_markup=settings_keyboard(user_id))

    elif message.text == f"{la['change_name']}":
        bot.send_message(user_id, f"{la['Enter_your_name']}")
        user.user_step = 4
        user.save()


    elif user.user_step == '4' and message.text  not in  [f"{la['change_phone']}", f"{la['change_name']}", f"{la['back1']}" ] :
        name = message.text.replace(" ", "")
        name = all(map(str.isalpha, name))
        if name == True:
            user.name = message.text
            user.user_step = 0
            user.save()
            bot.send_message(user_id, f"{la['name_regis2']} {user.name}")
        else:
            bot.send_message(user_id, f"{la['name_regis1']}")


    elif message.text == f"{la['change_phone']}":
        keyboard = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True, row_width=1)
        keyboard.add(*[
            KeyboardButton(text=f"{la['keybord_kontact']}", request_contact=True),
            KeyboardButton(text=f"{la['back1']}"),
                       ])
        bot.send_message(user_id, f"{la['phone_regis']}", reply_markup=keyboard)
        user.user_step = 5
        user.save()

    elif user.user_step == '5':
        if message.contact:
            phone = message.contact.phone_number
            if phone[0] == "+":
                user.phone_number = message.contact.phone_number
            else:
                phone = "+" + str(phone)
                user.phone_number = phone
            user.user_step = 0
            user.save()
            try:
                keyboard = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
                keyboard.add(*[KeyboardButton(f"{la['change_name']}"),
                               KeyboardButton(f"{la['change_phone']}"),
                               KeyboardButton(f"{la['back1']}")])
                user.save()
                bot.send_message(chat_id=user_id, text=f"{la['change_phone1']}", reply_markup=keyboard)
                bot.send_message(user_id, f"{la['Personal_information']} \n\n {la['last_first_name']}: {user.name}"
                                          f"\n\n{la['phone']}: {user.phone_number}")
            except:
                bot.send_message(chat_id=user_id, text=f"📱 Bu raqam registratsiyadan o'tgan ☹️\n"
                                          f" iltimos tekshirib qayta yuboring 🔄")

        elif message.text != f"{la['back1']}":
            phone = message.text.replace(' ', '').replace('+', '')
            if phone.isdigit() == True:
                if phone[0: 3] == '998' and len(phone) == 12:
                    phone = "+" + str(phone)
                    user.phone_number = phone
                    try:
                        keyboard = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
                        keyboard.add(*[KeyboardButton(f"{la['change_name']}"),
                                       KeyboardButton(f"{la['change_phone']}"),
                                       KeyboardButton(f"{la['back1']}")])
                        user.user_step = 4
                        user.save()
                        bot.send_message(chat_id=user_id, text=f"{la['change_phone1']}", reply_markup=keyboard)
                        bot.send_message(user_id,
                                         f"{la['Personal_information']} \n\n {la['last_first_name']}: {user.name}"
                                         f"\n\n{la['phone']}: {user.phone_number}")
                    except:
                        bot.send_message(chat_id=user_id,
                                         text=f"📱 Bu raqam registratsiyadan o'tgan ☹️\n"
                                              f" iltimos tekshirib qayta yuboring 🔄")
                elif len(phone) == 9:
                    phone = "+998" + str(phone)
                    user.phone_number = phone
                    user.user_step = 4
                    try:
                        keyboard = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
                        keyboard.add(*[KeyboardButton(f"{la['change_name']}"),
                                       KeyboardButton(f"{la['change_phone']}"),
                                       KeyboardButton(f"{la['back1']}")])
                        user.save()
                        bot.send_message(chat_id=user_id, text=f"{la['change_phone1']}", reply_markup=keyboard)
                        bot.send_message(user_id, f"{la['Personal_information']} \n\n {la['last_first_name']}: {user.name}"
                                                  f"\n\n{la['phone']}: {user.phone_number}")
                    except:
                        bot.send_message(chat_id=user_id,
                                         text=f"📱 Bu raqam registratsiyadan o'tgan ☹️\n"
                                              f" iltimos tekshirib qayta yuboring 🔄")
                else:
                    bot.send_message(user_id, text=f"{la['phone_regis1']} {user.name}", reply_markup=contact_keyboard())

            else: bot.send_message(user_id, text=f"{la['phone_regis1']} {user.name}")

        elif message.text == f"{la['back1']}":
            keyboard = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
            keyboard.add(*[KeyboardButton(f"{la['change_name']}"),
                           KeyboardButton(f"{la['change_phone']}"),
                           KeyboardButton(f"{la['back1']}")])
            user.user_step = 0
            user.save()
            bot.send_message(chat_id=user_id, text=f"{message.text}", reply_markup=keyboard)

        else:
            bot.send_message(user_id, text=f"{la['phone_regis1']} {user.name}")

    elif message.text == '🛒Savat':
        s = 0
        n = len(user_cart)
        qty = user_cart[s].qty
        book_name = user_cart[s].book_name
        text = f"📕: {user_cart[s].book_name}\n\n" \
               f"💴 narxi: {qty} ta x {book_name.price} = {user_cart[s].final_price} so'm"
        final_price = 0
        for price in user_cart:
            final_price += price.final_price
        keyboard = InlineKeyboardMarkup(row_width=4)
        keyboard.add(*[
            InlineKeyboardButton(text='-', callback_data=f'decrementt_{book_name}_{s}'),
            InlineKeyboardButton(text=f'{qty}', callback_data=f'soni_{book_name}_'),
            InlineKeyboardButton(text='+', callback_data=f'incrementt_{book_name}_{s}'),
            InlineKeyboardButton(text='❌', callback_data=f'delet_{book_name}_{s}'),
        ])
        keyboard.add(*[
            InlineKeyboardButton(text='⏪', callback_data=f'backnext_{book_name}_{s}'),
            InlineKeyboardButton(text=f'{s + 1}/{n}', callback_data="__"),
            InlineKeyboardButton(text='⏩', callback_data=f'next_{book_name}_{s}'),
        ])
        keyboard.row(*[
            InlineKeyboardButton(text=f'💴 Umumiy narx: {final_price} so\'m', callback_data="__")
        ])

        keybord = ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
        keybord.add(*[KeyboardButton(text="✅ Tasdiqlash"),
                      KeyboardButton(text="🛒 ❌ Savatni tozalash")])
        keybord.add(*[KeyboardButton(text="🏠 Menu")])
        bot.send_message(message.chat.id, message.text, reply_markup=keybord)
        user.user_step = 10
        user.message_id = message.message_id + 2
        user.save()
        bot.send_photo(message.chat.id, photo=user_cart[s].image, caption=text, reply_markup=keyboard)

    elif message.text == "✅ Tasdiqlash" and user.user_step == '10':

        customer = Customer.objects.get(user_id=user_id)
        owner, _ = Owner.objects.get_or_create(customer=customer)
        user_cart = User_cart.objects.filter(user_id=customer)
        Savat = [""]
        final_price = 0
        for i in range(0, len(user_cart)):
            final_price = final_price + user_cart[i].final_price
            text = f"{i + 1}) 📘  {user_cart[i].book_name}    {user_cart[i].qty} x {user_cart[i].book_name.price} so'm\n\t" \
                   f"----------------------------------\n"
            Savat.append(text)
            owner.total_products = i + 1
        text = ' '.join(Savat)
        owner.products = text
        owner.final_price = final_price
        owner.save()
        keyboard = ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
        keyboard.add(*[KeyboardButton(text="📍 Geolakatsiyamni yuborish",request_location=True)])
        bot.delete_message(user_id, message_id=user.message_id)
        bot.send_message(message.chat.id, "📍 Geolakatsiyangizni yuboring", reply_markup=keyboard)

    elif message.text == "🏠 Menu":
        bot.delete_message(chat_id=message.from_user.id, message_id=user.message_id)
        bot.send_message(message.from_user.id, message.text, reply_markup=menu_keyboard(user_id))

    elif message.text == "🛒 ❌ Savatni tozalash":
        cart = User_cart.objects.filter(user_id=customer)
        cart.delete()
        bot.delete_message(user_id, message_id=user.message_id)
        bot.send_message(user_id, "🛒Savatingiz tozalandi", reply_markup=menu_keyboard(user_id))

    elif message.text == f"{la['back1']}":
        bot.send_message(user_id, text=f"{la['back1']}", reply_markup=menu_keyboard(user_id))


class Command(BaseCommand):
    help = "telegram-bot"

    def handle(self, *args, **options):
        print(bot.get_me())
        bot.polling()