# Generated by Django 3.2.4 on 2021-06-28 06:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('adminbot', '0015_auto_20210628_0010'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='admin',
            options={'verbose_name_plural': 'Admin'},
        ),
        migrations.AlterModelOptions(
            name='connect_with_us',
            options={'verbose_name_plural': "Biz bilan bog'lanish ⚙️"},
        ),
        migrations.AlterField(
            model_name='admin',
            name='admin_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='adminbot.customer', unique=True, verbose_name='Admin ID'),
        ),
        migrations.AlterField(
            model_name='admin',
            name='name_admin',
            field=models.CharField(blank=True, max_length=255, null=True, unique=True, verbose_name='admin ismi'),
        ),
    ]
