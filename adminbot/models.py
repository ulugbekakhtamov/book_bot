from django.db import models
from django.utils import timezone
#from phonenumber_field.modelfields import PhoneNumberField

class Customer(models.Model):

    user_id = models.PositiveIntegerField(verbose_name='Mijoz ID si ')
    name = models.TextField(verbose_name='Mijoz ismi',)
    phone_number = models.CharField(verbose_name='Telefon raqami', max_length=50 , blank=True , null=True , unique=True)
    language = models.TextField(verbose_name='til', max_length=2)
    user_step = models.CharField(max_length=50, null=True, blank=True)
    message_id = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = "Клиент 👥"
        verbose_name_plural = "Mijozlar 👥"

class Book_categories(models.Model):

    book_categories_name = models.CharField(verbose_name='Kitobning toyfasi', max_length=255, unique=True)
    image = models.ImageField(verbose_name="Kitob rasmi")

    def __str__(self):
        return self.book_categories_name

    class Meta:
        verbose_name = "Категория книг 📚"
        verbose_name_plural = "Kitob toyfalari 📚"


class Books(models.Model):

    category = models.ForeignKey(Book_categories, verbose_name='Toyfasi', on_delete=models.CASCADE)
    title = models.CharField(max_length=255, verbose_name="Nomi", unique=True)
    image = models.ImageField(verbose_name="Rasmi")
    language = models.CharField(verbose_name="Til", max_length=255)
    year = models.SmallIntegerField(verbose_name="yili", blank=True, null=True)
    author = models.CharField(max_length=255, verbose_name="Muallif(lar)")
    translator = models.CharField(max_length=255, verbose_name="Tarjimon(lar)", null=True, blank=True)
    cover = models.CharField(max_length=255, verbose_name="Muqovasi", null=True, blank=True)
    about_the_book = models.TextField(max_length=255, verbose_name="Kitob haqida", blank=True, null=True)
    price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Цена")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Книг 📚"
        verbose_name_plural = " Kitoblar 📚"

class User_cart(models.Model):

    user_id = models.ForeignKey(Customer, verbose_name="Mijoz", on_delete=models.CASCADE)
    book_name = models.ForeignKey(Books, verbose_name="Kitob", on_delete=models.CASCADE)
    image = models.ImageField(verbose_name="Rasm", null=True, blank=True)
    qty = models.PositiveIntegerField(default=1)
    final_price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Umumiy narx:")

    def __str__(self):
         return f"Продукт: {self.user_id.name}|{self.book_name.title}"

    def save(self, *args, **kwargs):
        self.final_price = self.qty * self.book_name.price
        super().save(*args, **kwargs)



class Owner(models.Model):

    customer = models.ForeignKey(Customer, verbose_name="Xaridor", on_delete=models.CASCADE)
    adress = models.CharField(max_length=255, verbose_name="adress", null=True, blank=True)
    latitude = models.FloatField(verbose_name= 'Latitude', blank=True, null=True)
    longitude = models.FloatField(verbose_name= 'Longitude', blank=True, null=True)
    products = models.TextField(max_length=255, verbose_name="mahsulotlar", null=True, blank=True)
    total_products = models.PositiveIntegerField(default=0, verbose_name="jami mahsulotlar")
    final_price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Umumiy narx", null=True, blank=True)

    def __str__(self):
        return f"{self.customer}"

class Orders(models.Model):

    name = models.ForeignKey(Customer, verbose_name="Xaridor ismi", on_delete=models.CASCADE)
    orders = models.TextField(max_length=255, verbose_name="Mahsulotlar", null=True, blank=True)
    phone_number = models.CharField(verbose_name='Telefon raqam', max_length=50 , blank=True , null=True )
    adress = models.CharField(max_length=255, verbose_name="adress", null=True, blank=True)
    final_price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Umumiy narx", null=True, blank=True)
    latitude = models.FloatField(verbose_name='Latitude', blank=True, null=True)
    longitude = models.FloatField(verbose_name='Longitude', blank=True, null=True)
    status = models.CharField(max_length=255, verbose_name="status", null=True, blank=True)
    created = models.DateTimeField(default='0000-00-00', editable=False, null=True, blank=True)
    modified = models.DateTimeField(null=True, blank=True)


    def __str__(self, *args, **kwargs):
        return f"{self.id}"

    def save(self, *args, **kwargs):

        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(Orders, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Заказ 🛍"
        verbose_name_plural = "Buyurtmalar 🛍"

class Admin(models.Model):

    admin_id = models.OneToOneField(Customer, verbose_name="Admin ID", unique=True, on_delete=models.CASCADE)
    name_admin = models.CharField(max_length=255, verbose_name="admin ismi", unique=True, blank=True, null=True)

    def __str__(self, *args, **kwargs):
        return f"{self.name_admin}"

    class Meta:
        verbose_name = "Админ "
        verbose_name_plural = "Adminlar 🔑"

class Connect_with_us(models.Model):

    connect_with_us = models.TextField(max_length=255, verbose_name="Biz bilan bog'lanish ⚙️")

    def __str__(self, *args, **kwargs):
        return f"{self.connect_with_us}"

    class Meta:
        verbose_name_plural = "Biz bilan bog'lanish ⚙️"

