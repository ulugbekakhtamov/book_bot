from django.contrib import admin

from .models import Customer, Book_categories, \
    Books, User_cart, Owner, Orders, Admin, Connect_with_us

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'name', 'phone_number')
    search_fields = ('user_id', 'name', 'phone_number' )

@admin.register(Books)
class BooksAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'price')
    list_filter = ['category']
    ordering = ('category', 'price', 'title')
    search_fields = ['title', 'category__book_categories_name', 'price']

@admin.register(Orders)
class OrdersAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'phone_number', 'final_price')
    search_fields = ('id', 'name__name', 'phone_number')

admin.site.register(Book_categories)

admin.site.register(Admin)
admin.site.register(Connect_with_us)

# admin.site.register(User_cart)
# admin.site.register(Owner)





