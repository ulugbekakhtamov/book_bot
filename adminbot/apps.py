from django.apps import AppConfig


class AdminbotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'adminbot'
